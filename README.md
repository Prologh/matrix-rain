# Matrix Rain

README / 21 March 2020

-----

## Introduction

Matrix Rain is a console application written in C# implementing symbols rain based on the famous Wachowskis Trilogy — ["The Matrix"](https://www.imdb.com/title/tt0133093/).

**Note:** project is still in development...

![zero matrix](https://gitlab.com/Prologh/matrix-rain/-/raw/master/img/zero-matrix.png)

## Technical information

Name | Value
--- | ---
Main programming language | C#
Framework targeted | .NET Core 3.1

## License

Licensed under MIT. Read full license [here](https://gitlab.com/matrix-rain/raw/master/LICENSE).

## Credits

**Piotr Wosiek** | [GitLab](https://gitlab.com/Prologh) | [GitHub](https://github.com/Prologh)
