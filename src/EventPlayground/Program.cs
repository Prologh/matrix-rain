﻿using Pastel;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Timers;

namespace EventPlayground
{
    class Program
    {
        private readonly static int MAX_X = 100;
        private readonly static int MAX_Y = 30;
        private readonly static int TRICKLES_COUNT = 200;
        private readonly static Color MAIN_COLOR = Color.Green;

        private readonly static object consoleLock = new object();

        readonly static Dictionary<int, (Point Point, Color Color)> heads = new Dictionary<int, (Point, Color)>();

        static void Main()
        {
            Console.CursorVisible = false;
            Console.SetWindowSize(MAX_X, MAX_Y);

            var timers = new List<Timer>(capacity: TRICKLES_COUNT);

            for (var i = 0; i < TRICKLES_COUNT; i++)
            {
                var id = i;
                var velocitySpan = 300;
                var velocityStartingLevel = 35;
                var velocity = new Random().Next(velocityStartingLevel, velocityStartingLevel + velocitySpan);
                var randomPoint = GetRandomPoint();
                var brightnessMultiplayer = 1 - ((velocity - velocityStartingLevel) / ((double)velocitySpan));
                var shadeOfGreen = GetShadeOfMainColor(brightnessMultiplayer);
                heads[i] = (randomPoint, shadeOfGreen);
                var timer = new Timer(TimeSpan.FromMilliseconds(velocity).TotalMilliseconds);
                timer.Elapsed += (s, e) => Elapsed(s, e, id);
                timers.Add(timer);
            }

            timers.ForEach(timer => timer.Start());

            do
            {
                System.Threading.Thread.Sleep(TimeSpan.FromMilliseconds(1));
            } while (true);
        }

        private static void Elapsed(object sender, ElapsedEventArgs e, int id)
        {
            var oldTuple = heads[id];

            CleanTrickleLastCell(oldTuple.Point);

            var newPoint = MoveTrickle(velocity: 1, oldTuple.Point);
            heads[id] = (newPoint, oldTuple.Color);

            PrintRainTrickle('0', newPoint, oldTuple.Color);
        }

        private static Point MoveTrickle(int velocity, Point yellowHead)
        {
            if (yellowHead.Y + velocity >= MAX_Y)
            {
                return new Point(yellowHead.X, 0);
            }
            else
            {
                return new Point(yellowHead.X, yellowHead.Y + velocity);
            }
        }

        private static void PrintRainTrickle(char symbol, Point trickleHead, Color color)
        {
            lock (consoleLock)
            {
                Console.SetCursorPosition(trickleHead.X, trickleHead.Y);
                Console.Write(symbol.ToString().Pastel(color));
                Console.ForegroundColor = ConsoleColor.White;
            }
        }

        private static void CleanTrickleLastCell(Point point)
        {
            lock (consoleLock)
            {
                Console.SetCursorPosition(point.X, point.Y);
                Console.Write(' ');
            }
        }

        private static Color GetRandomColor()
        {
            var random = new Random();

            return Color.FromArgb(random.Next(256), random.Next(256), random.Next(256));
        }

        private static Point GetRandomPoint()
        {
            var random = new Random();
            var x = random.Next(MAX_X);
            var y = random.Next(MAX_Y);

            return new Point(x, y);
        }

        private static Color GetShadeOfMainColor(double brightness)
        {
            var baseColor = MAIN_COLOR;

            return Color.FromArgb(
                baseColor.A,
                (int)(baseColor.R * brightness),
                (int)(baseColor.G * brightness),
                (int)(baseColor.B * brightness));
        }
    }
}
