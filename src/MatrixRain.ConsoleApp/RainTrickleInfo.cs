﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace MatrixRain.ConsoleApp
{
    public class RainTrickleInfo
    {
        public RainTrickleInfo(int velocity, Point startPoint, IEnumerable<char> characters)
        {
            Velocity = velocity;
            StartPoint = startPoint;
            Characters = characters.ToList().AsReadOnly();
        }

        public IReadOnlyList<char> Characters { get; private set; }

        public int Column => StartPoint.X;

        public Point EndPoint => new Point(StartPoint.X, StartPoint.Y == 0 ? 0 : StartPoint.Y - Length + 1);

        public char Head => Characters.FirstOrDefault();

        public int Length => Characters.Count;

        public Point StartPoint { get; private set; }

        /// <summary>
        /// Velocity of a rain trickle measured in cells travelled per step.
        /// </summary>
        public int Velocity { get; }

        public void MoveForward()
        {
            StartPoint = new Point(Column, StartPoint.Y + Velocity);
        }

        public void RollBackUp()
        {
            StartPoint = new Point(Column, 0);
        }

        public void RollBackUp(IEnumerable<char> characters)
        {
            SetCharacters(characters);
            RollBackUp();
        }

        private void SetCharacters(IEnumerable<char> characters)
        {
            if (characters is null)
            {
                throw new ArgumentNullException(nameof(characters));
            }

            Characters = characters.ToList().AsReadOnly();
        }
    }
}
