﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace MatrixRain.ConsoleApp
{
    public class MatrixRainInfo
    {
        public MatrixRainInfo(IEnumerable<KeyValuePair<int, IEnumerable<RainTrickleInfo>>> tricklesDictionary)
        {
            if (tricklesDictionary is null)
            {
                throw new ArgumentNullException(nameof(tricklesDictionary));
            }

            TricklesDictionary = new ReadOnlyDictionary<int, IReadOnlyList<RainTrickleInfo>>(tricklesDictionary
                .Select(pair => new KeyValuePair<int, IReadOnlyList<RainTrickleInfo>>(
                    pair.Key, pair.Value.ToList().AsReadOnly()))
                .ToDictionary(pair => pair.Key, pair => pair.Value));
        }

        public TimeSpan DelayBetweenStep { get; set; }

        public int Height { get; set; }

        public IReadOnlyDictionary<int, IReadOnlyList<RainTrickleInfo>> TricklesDictionary { get; }

        public int Width { get; set; }
    }
}
