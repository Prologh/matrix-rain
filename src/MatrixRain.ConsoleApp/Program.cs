﻿using Pastel;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading;

namespace MatrixRain.ConsoleApp
{
    class Program
    {
        static void Main()
        {
            var width = Console.LargestWindowWidth;
            var height = Console.LargestWindowHeight;
            Console.SetWindowSize(width, height);
            Console.CursorVisible = false;

            var matrixRainInfo = InitializeMatrixRainInfo(width, height);

            do
            {
                WriteStep(matrixRainInfo);

                // Add more trickles or reset position of those completly out of window size.

                Thread.Sleep(matrixRainInfo.DelayBetweenStep);
            } while (true);
        }

        private static MatrixRainInfo InitializeMatrixRainInfo(
            int width, int height)
        {
            var trickles = InitializeRainTrickles(width, height);

            return new MatrixRainInfo(trickles)
            {
                Width = width,
                DelayBetweenStep = TimeSpan.FromMilliseconds(400),
                Height = height,
            };
        }

        private static IDictionary<int, IEnumerable<RainTrickleInfo>> InitializeRainTrickles(int width, int height)
        {
            var rainTrickles = new Dictionary<int, IEnumerable<RainTrickleInfo>>();

            for (var column = 0; column < width; column++)
            {
                rainTrickles[column] = IniliazieRainTricklesForColumn(column, height);
            }

            return rainTrickles;
        }

        private static IEnumerable<RainTrickleInfo> IniliazieRainTricklesForColumn(int column, int height)
        {
            var random = new Random();
            var columnTricklesCount = random.Next(1, 4);

            for (int trickle = 0; trickle < columnTricklesCount; trickle++)
            {
                var velocity = random.NextDouble() > 0.8 ? 2 : 1;
                var length = random.Next(5, 5 + height / 2);
                var startingPoint = new Point(column, random.Next(height) + length);
                var characters = GetRandomChars(length);

                yield return new RainTrickleInfo(velocity, startingPoint, characters);
            }
        }

        private static IEnumerable<char> GetRandomChars(int count)
        {
            for (int i = 0; i < count; i++)
            {
                yield return GetRandomChar();
            }
        }

        private static char GetRandomChar()
        {
            var random = new Random();

            return (char)random.Next(48, 91);
        }

        private static void WriteStep(MatrixRainInfo matrixRainConfiguration)
        {
            for (var column = 0; column < matrixRainConfiguration.Width; column++)
            {
                var columnTrickles = matrixRainConfiguration.TricklesDictionary[column];

                foreach (var trickle in columnTrickles)
                {
                    if (trickle.EndPoint.Y >= matrixRainConfiguration.Height)
                    {
                        trickle.RollBackUp(GetRandomChars(trickle.Length));
                    }
                    else
                    {
                        trickle.MoveForward();
                    }
                }

                foreach (var trickle in columnTrickles)
                {
                    WriteColumnRainTrickle(trickle, matrixRainConfiguration.Height);
                }
            }
        }

        private static void WriteColumnRainTrickle(RainTrickleInfo trickleInfo, int height)
        {
            var index = 0;
            var row = trickleInfo.EndPoint.Y - trickleInfo.Velocity;

            while (row <= trickleInfo.StartPoint.Y && index < trickleInfo.Length)
            {
                var currentPosition = new Point(trickleInfo.Column, row);

                if (row < 0)
                {
                    row++;

                    if (row >= trickleInfo.EndPoint.Y)
                    {
                        index++;
                    }

                    continue;
                }

                if (row >= height)
                {
                    break;
                }

                if (row < trickleInfo.EndPoint.Y)
                {
                    WriteSymbol(' ', Color.White, currentPosition);
                }
                else
                {
                    var symbol = trickleInfo.Characters[index];
                    var color = index == trickleInfo.Length - 1 ? Color.LightGreen : Color.Green;

                    WriteSymbol(symbol, color, currentPosition);

                    index++;
                }

                row++;
            };
        }

        private static void WriteSymbol(char symbol, Color color, Point point)
        {
            // TODO:
            // Foreground color

            Console.SetCursorPosition(point.X, point.Y);
            Console.Write(symbol.ToString().Pastel(color));
            Console.ResetColor();
        }
    }
}
